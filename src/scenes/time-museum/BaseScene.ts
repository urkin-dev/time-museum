import * as utils from '@dcl/ecs-scene-utils'
import { Button, Elevator, ElevatorDoor, BaseEntity, Like } from './entities/index'
import { GameDataInstance } from './store/GameData'
import type { eraType } from './store/GameData'
import resources from './resources'

export class BaseSceneBuilder {
	private currentEra: eraType = 'stone-age'
	private elevator: Elevator
	private leftDoor: ElevatorDoor
	private rightDoor: ElevatorDoor

	constructor() {
		this.elevator = new Elevator(
			resources.models.elevatorCabin,
			new Transform({ position: new Vector3(28, 0, 4).add(GameDataInstance.offsetVecor) })
		)
		this.elevator.getComponent(Transform).rotation.setEuler(0, -45, 0)

		this.leftDoor = new ElevatorDoor(
			resources.models.elevatorDoor1,
			new Transform({ position: Vector3.Zero() }),
			'left',
			0.5,
			new Vector3(0.8, 0, 0)
		)
		this.leftDoor.setParent(this.elevator)

		this.rightDoor = new ElevatorDoor(
			resources.models.elevatorDoor2,
			new Transform({ position: Vector3.Zero() }),
			'left',
			0.5,
			new Vector3(-0.8, 0, 0)
		)
		this.rightDoor.setParent(this.elevator)

		this.setTrigger()
		this.createButtons()
		this.createBorders()

		new Like(
			{
				position: new Vector3(26, 1, 2).add(GameDataInstance.offsetVecor),
				rotation: Quaternion.Euler(0, 135, 0)
			},
			'61b906d5dd08def8380abac0'
		)
	}

	private setTrigger() {
		let triggerBoxShape = new utils.TriggerBoxShape(new Vector3(10, 10, 10), Vector3.Zero())

		this.elevator.addComponent(
			new utils.TriggerComponent(triggerBoxShape, {
				onCameraExit: () => {
					this.rightDoor.closeDoor()
					this.leftDoor.closeDoor()
				},
				onCameraEnter: () => {
					this.rightDoor.openDoor()
					this.leftDoor.openDoor()
				}
			})
		)
	}

	private closeDoorsWithDelay() {
		return new Promise((resolve) => {
			this.rightDoor.closeDoor()
			this.leftDoor.closeDoor()
			// Imitate close door animation delay
			utils.setTimeout(2000, resolve)
		})
	}

	private async goToEgypt() {
		if (GameDataInstance.era !== 'egypt') {
			await this.closeDoorsWithDelay()
			await GameDataInstance.clearEntities()
			await GameDataInstance.changeEra('egypt')
			this.rightDoor.openDoor()
			this.leftDoor.openDoor()
		}
	}

	private async goToGreek() {
		if (GameDataInstance.era !== 'greek') {
			await this.closeDoorsWithDelay()
			await GameDataInstance.clearEntities()
			await GameDataInstance.changeEra('greek')
			this.leftDoor.openDoor()
			this.rightDoor.openDoor()
		}
	}

	private async goToStoneAge() {
		if (GameDataInstance.era !== 'stone-age') {
			await this.closeDoorsWithDelay()
			await GameDataInstance.clearEntities()
			await GameDataInstance.changeEra('stone-age')
			this.leftDoor.openDoor()
			this.rightDoor.openDoor()
		}
	}

	private async goToAgeOfDiscovery() {
		if (GameDataInstance.era !== 'ageOfDiscovery') {
			await this.closeDoorsWithDelay()
			await GameDataInstance.clearEntities()
			await GameDataInstance.changeEra('ageOfDiscovery')
			this.leftDoor.openDoor()
			this.rightDoor.openDoor()
		}
	}

	private async goToMedieval() {
		if (GameDataInstance.era !== 'medieval') {
			await this.closeDoorsWithDelay()
			await GameDataInstance.clearEntities()
			await GameDataInstance.changeEra('medieval')
			this.leftDoor.openDoor()
			this.rightDoor.openDoor()
		}
	}

	private createBorders() {
		const borderEgypt = new BaseEntity(
			resources.models.borderEgypt,
			new Transform({ scale: new Vector3(0.1, 0.1, 0.1), position: new Vector3(0.85, 2.05, 0.2) })
		)
		borderEgypt.getComponent(Transform).rotation.setEuler(0, -90, 0)
		borderEgypt.setParent(this.elevator)

		const borderStoneAge = new BaseEntity(
			resources.models.borderStoneAge,
			new Transform({ scale: new Vector3(0.1, 0.1, 0.1), position: new Vector3(0.85, 2.05, 0.2) })
		)
		borderStoneAge.getComponent(Transform).rotation.setEuler(0, -90, 0)
		borderStoneAge.setParent(this.elevator)

		const borderGreece = new BaseEntity(
			resources.models.borderGreece,
			new Transform({ scale: new Vector3(0.1, 0.1, 0.1), position: new Vector3(0.85, 2.05, 0.2) })
		)
		borderGreece.getComponent(Transform).rotation.setEuler(0, -90, 0)
		borderGreece.setParent(this.elevator)

		const borderDiscovery = new BaseEntity(
			resources.models.borderDiscovery,
			new Transform({ scale: new Vector3(0.1, 0.1, 0.1), position: new Vector3(0.85, 1.55, 1.1) })
		)
		borderDiscovery.getComponent(Transform).rotation.setEuler(0, -90, 0)
		borderDiscovery.setParent(this.elevator)

		const borderMiddleAges = new BaseEntity(
			resources.models.borderMiddleAges,
			new Transform({ scale: new Vector3(0.1, 0.1, 0.1), position: new Vector3(0.85, 1.55, 1.1) })
		)
		borderMiddleAges.getComponent(Transform).rotation.setEuler(0, -90, 0)
		borderMiddleAges.setParent(this.elevator)
	}

	private createButtons() {
		const buttonToEgypt = new Button(
			resources.models.button,
			new Transform({ scale: new Vector3(0.2, 0.2, 0.2), position: new Vector3(0.83, 2, -0.1) })
		)
		buttonToEgypt.getComponent(Transform).rotation.setEuler(90, 90, 0)
		buttonToEgypt.setParent(this.elevator)
		buttonToEgypt.onPress(this.goToEgypt.bind(this), 'Go to Egypt')

		const buttonToStoneEra = new Button(
			resources.models.button,
			new Transform({ scale: new Vector3(0.2, 0.2, 0.2), position: new Vector3(0.83, 2, 0.23) })
		)
		buttonToStoneEra.getComponent(Transform).rotation.setEuler(90, 90, 0)
		buttonToStoneEra.setParent(this.elevator)
		buttonToStoneEra.onPress(this.goToStoneAge.bind(this), 'Go to Stone Era')

		const buttonToGreek = new Button(
			resources.models.button,
			new Transform({ scale: new Vector3(0.2, 0.2, 0.2), position: new Vector3(0.83, 2, -0.43) })
		)
		buttonToGreek.getComponent(Transform).rotation.setEuler(90, 90, 0)
		buttonToGreek.setParent(this.elevator)
		buttonToGreek.onPress(this.goToGreek.bind(this), 'Go to Greece')

		const buttonToMedieval = new Button(
			resources.models.button,
			new Transform({ scale: new Vector3(0.2, 0.2, 0.2), position: new Vector3(0.82, 1.52, 0.1) })
		)
		buttonToMedieval.getComponent(Transform).rotation.setEuler(90, 90, 0)
		buttonToMedieval.setParent(this.elevator)
		buttonToMedieval.onPress(this.goToMedieval.bind(this), 'Go to Medieval')

		const buttonToAgeOfDiscovery = new Button(
			resources.models.button,
			new Transform({ scale: new Vector3(0.2, 0.2, 0.2), position: new Vector3(0.82, 1.52, -0.24) })
		)
		buttonToAgeOfDiscovery.getComponent(Transform).rotation.setEuler(90, 90, 0)
		buttonToAgeOfDiscovery.setParent(this.elevator)
		buttonToAgeOfDiscovery.onPress(this.goToAgeOfDiscovery.bind(this), 'Go to Age of Discovery')
	}

	public build() {
		GameDataInstance.changeEra(this.currentEra)
	}
}
