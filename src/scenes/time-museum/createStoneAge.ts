import { BaseEntity, Bonfire } from './entities/index'
import { GameDataInstance } from './store/GameData'
import resources from './resources'

export function createStoneAge(offsetVecor: Vector3) {
	const ground = new BaseEntity(
		resources.models.snow,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(ground)

	const stoneAge = new BaseEntity(
		resources.models.stoneAge,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)

	GameDataInstance.addEntity(stoneAge)

	const bonfire = new Bonfire(
		resources.models.bonfire,
		new Transform({
			position: new Vector3(0, 0, 0).add(offsetVecor)
		})
	)
	GameDataInstance.addEntity(bonfire)
}
