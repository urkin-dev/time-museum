import { createEgypt, createGreek, createStoneAge, createMedieval, createAgeOfDiscovery } from '../index'

export type eraType = 'egypt' | 'greek' | 'stone-age' | 'medieval' | 'ageOfDiscovery'

class GameData {
	public era: eraType = 'stone-age'
	private entities: Entity[] = []
	private systems: ISystem[] = []
	public offsetVecor = new Vector3(0, 0, 0)

	public async changeEra(era: eraType) {
		new Promise(() => {
			this.era = era

			switch (era) {
				case 'egypt':
					createEgypt(this.offsetVecor)
					break
				case 'greek':
					createGreek(this.offsetVecor)
					break
				case 'stone-age':
					createStoneAge(this.offsetVecor)
					break
				case 'medieval':
					createMedieval(this.offsetVecor)
					break

				case 'ageOfDiscovery':
					createAgeOfDiscovery(this.offsetVecor)
					break
			}
		})
	}

	public async clearEntities() {
		new Promise(() => {
			for (const entity of this.entities) {
				engine.removeEntity(entity)
			}

			for (const system of this.systems) {
				engine.removeSystem(system)
			}

			this.entities = []
			this.systems = []
		})
	}

	public addEntity(entity: Entity) {
		this.entities.push(entity)
	}

	public removeEntity(entity: Entity) {
		this.entities = this.entities.filter((e) => e !== entity)
		engine.removeEntity(entity)
	}

	public addSystem(system: ISystem) {
		this.systems.push(system)
	}
}

export const GameDataInstance = new GameData()
