import { BaseEntity, Bonfire, Chest, Rat, ShipWheel } from './entities/index'
import { GameDataInstance } from './store/GameData'
import resources from './resources'

export function createAgeOfDiscovery(offsetVecor: Vector3) {
	const ground = new BaseEntity(
		resources.models.sea,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(ground)

	const discoveryAge = new BaseEntity(
		resources.models.discoveryAge,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(discoveryAge)

	const shipWheel = new ShipWheel(
		resources.models.wheelShip,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(shipWheel)

	const compass = new BaseEntity(
		resources.models.compass,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(compass)

	const bonfire = new Bonfire(
		resources.models.bonfire,
		new Transform({
			position: new Vector3(15, 0, 0).add(offsetVecor)
		})
	)
	GameDataInstance.addEntity(bonfire)

	const chest = new Chest(
		resources.models.chest,
		new Transform({
			position: new Vector3(22, 0, 31).add(offsetVecor),
			scale: new Vector3(0.5, 0.5, 0.5)
		})
	)
	const rat = new Rat(
		resources.models.rat,
		new Transform({
			position: new Vector3(-8, 0.7, -6),
			scale: new Vector3(0.5, 0.5, 0.5)
		})
	)
	rat.getComponent(Transform).rotation.setEuler(0, 90, 0)
	rat.setParent(chest)

	GameDataInstance.addEntity(chest)
}
