import { NPC } from '@dcl/npc-scene-utils'
import { config } from './config'
import { NpcDialog } from './dialog'

export const npc = new NPC(
	{ position: config.position, rotation: config.rotation },
	'models/time-museum/npc.glb',
	() => {
		npc.playAnimation('RatChat2')
		npc.talk(NpcDialog)
	},
	{
		hoverText: config.hovertext,
		onlyClickTrigger: true,
		portrait: {
			path: `images/time-museum/portraits/${config.portraits}`,
			height: 256,
			width: 256,
			section: {
				sourceHeight: 512,
				sourceWidth: 512
			}
		},
		faceUser: true,
		idleAnim: 'RatIdle2',
		onWalkAway: () => {
			npc.playAnimation('RatIdle2')
		}
	}
)
