import { GameDataInstance } from '../store/GameData'

export const config = {
	hovertext: 'Talk with Rat',
	portraits: 'npc.png',
	position: new Vector3(30, 0, 7).add(GameDataInstance.offsetVecor),
	rotation: new Quaternion(0, -1, 0)
}
