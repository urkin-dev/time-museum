import { Dialog } from '@dcl/npc-scene-utils'
import { npc } from './Npc'

export const NpcDialog: Dialog[] = [
	// #0
	{
		text: 'Squeak, squeak, welcome to Time Museum!',
		audio: 'sounds/time-museum/npc3.mp3',
		triggeredByNext: () => npc.playAnimation('RatChat')
	},
	// #1
	{
		text: 'Would you like to learn more about this place?',
		isQuestion: true,
		buttons: [
			{ label: 'Yes', goToDialog: 3, triggeredActions: () => npc.playAnimation('RatChat2') },
			{ label: 'No', goToDialog: 2, triggeredActions: () => npc.playAnimation('RatChat2') }
		],
		audio: 'sounds/time-museum/npc1.mp3'
	},
	// #2
	{
		text: "If you need my help, I'll be here, Squeak!",
		triggeredByNext: () => npc.playAnimation('RatIdle2'),
		isEndOfDialog: true,
		audio: 'sounds/time-museum/npc3.mp3'
	},
	// #3
	{
		text: 'I was running around looking for food, when I found this strange elevator.',
		audio: 'sounds/time-museum/npc2.mp3',
		triggeredByNext: () => npc.playAnimation('RatChat')
	},
	// #4
	{
		text: 'This elevator teleported me in other places, and different times, so I grew up and learned to talk. Just look at me, ha-ha!!',
		audio: 'sounds/time-museum/npc1.mp3',
		triggeredByNext: () => npc.playAnimation('RatChat2')
	},
	// #5
	{
		text: 'Try to use this machine it is pretty simple, but be careful, you know.',
		triggeredByNext: () => npc.playAnimation('RatChat'),
		audio: 'sounds/time-museum/npc2.mp3'
	},
	// #6
	{
		text: "Also you might find other versions of me, before I grew up, so don't worry, they were friendly as I am now. Good luck! Squeak",
		audio: 'sounds/time-museum/npc3.mp3',
		triggeredByNext: () => npc.playAnimation('RatIdle2'),
		isEndOfDialog: true
	}
]
