import { BaseEntity, TrojanHorse } from './entities/index'
import { GameDataInstance } from './store/GameData'
import resources from './resources'

export function createGreek(offsetVecor: Vector3) {
	const ground = new BaseEntity(
		resources.models.grass,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(ground)

	const greekAge = new BaseEntity(
		resources.models.greekAge,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(greekAge)

	const trojanHorse = new TrojanHorse(
		resources.models.trojanHorse,
		{ position: new Vector3(30, 3.5, 25).add(offsetVecor) },
		new Vector3(-20, 0, 0),
		5
	)
	GameDataInstance.addEntity(trojanHorse)
}
