import * as utils from '@dcl/ecs-scene-utils'
import { MovableEntity } from './MovableEntity'

export class Coconut extends MovableEntity {
	constructor(model: GLTFShape, transform: TranformConstructorArgs, deltaPosition: Vector3, duration: double) {
		super(model, transform, deltaPosition, duration)
		engine.addEntity(this)

		this.addComponent(
			new OnPointerDown((): void => {
				this.getComponent(utils.ToggleComponent).toggle()
				this.removeComponent(OnPointerDown)
			})
		)
	}
}
