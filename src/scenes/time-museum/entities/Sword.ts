const Z_OFFSET = 1.5

export class Sword extends Entity {
	isGrabbed = false

	private probability = 0.01

	constructor(model: GLTFShape, transform: Transform) {
		super()
		engine.addEntity(this)
		this.addComponent(model)
		this.addComponent(transform)

		this.addComponent(new Animator())
		this.getComponent(Animator).addClip(new AnimationState('unsuccessful', { looping: false }))

		this.addComponent(
			new OnPointerDown(
				() => {
					const isLucky = Math.random() < this.probability
					if (!this.isGrabbed && isLucky) {
						this.isGrabbed = true

						const transform = this.getComponent(Transform)
						// Calculates the sword's position relative to the camera
						transform.position = new Vector3(0.4, 1.6, -0.7)
						transform.scale = new Vector3(0.5, 0.5, 0.5)
						transform.rotation.setEuler(180, 90, -10)
						transform.position.z += Z_OFFSET
						this.setParent(Attachable.AVATAR)
					} else {
						this.shake()
					}
				},
				{ hoverText: 'Try to pull out!' }
			)
		)
	}

	private shake() {
		this.getComponent(Animator).getClip('unsuccessful').play()
	}
}
