export class Chest extends Entity {
	private isOpened = false

	constructor(model: GLTFShape, transform: TranformConstructorArgs) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		this.addComponent(transform)

		this.addComponent(new Animator())
		this.getComponent(Animator).addClip(new AnimationState('CloseChest', { looping: false }))
		this.getComponent(Animator).addClip(new AnimationState('OpenChest', { looping: false }))
		this.getComponent(Animator).getClip('CloseChest').play()

		this.addComponent(
			new OnPointerDown(() => {
				if (!this.isOpened) {
					this.open()
					this.isOpened = true
				} else {
					this.close()
					this.isOpened = false
				}
			})
		)
	}

	private close() {
		this.getComponent(Animator).getClip('CloseChest').play()
	}

	private open() {
		this.getComponent(Animator).getClip('OpenChest').play()
	}
}
