export class Button extends Entity {
	constructor(model: GLTFShape, transform: TranformConstructorArgs) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		this.addComponent(transform)

		this.addComponent(new Animator())
		this.getComponent(Animator).addClip(new AnimationState('ButtonPress', { looping: false }))
	}

	onPress(callback?: () => void, hintText = 'Press') {
		this.addComponent(
			new OnPointerDown(
				() => {
					this.getComponent(Animator).getClip('ButtonPress').play()
					callback && callback()
				},
				{ hoverText: hintText, distance: 1 }
			)
		)
	}
}
