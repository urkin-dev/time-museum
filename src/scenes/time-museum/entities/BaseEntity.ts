export class BaseEntity extends Entity {
	constructor(model: GLTFShape, transform?: TranformConstructorArgs) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		if (transform) {
			this.addComponent(transform)
		}
	}
}
