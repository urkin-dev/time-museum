export class Bonfire extends Entity {
	private lighted = true

	constructor(model: GLTFShape, transform: TranformConstructorArgs) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		this.addComponent(transform)

		this.addComponent(new Animator())
		this.getComponent(Animator).addClip(new AnimationState('BonfireBurn', { looping: true }))
		this.getComponent(Animator).addClip(new AnimationState('BonfireIdle', { looping: true }))
		this.light()

		this.addComponent(
			new OnPointerDown(() => {
				if (this.lighted) {
					this.putOut()
				} else {
					this.light()
				}
			})
		)
	}

	light() {
		this.getComponent(Animator).getClip('BonfireBurn').play()
		this.lighted = true
	}

	putOut() {
		this.getComponent(Animator).getClip('BonfireIdle').play()
		this.lighted = false
	}
}
