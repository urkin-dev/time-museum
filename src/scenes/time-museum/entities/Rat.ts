import * as utils from '@dcl/ecs-scene-utils'
import { GameDataInstance } from '../store/GameData'

export class Rat extends Entity {
	private patrolSystem: PatrolPath | undefined
	private endPath: Path3D | undefined

	constructor(model: GLTFShape, transform: Transform, endRatPath?: Path3D) {
		super()
		engine.addEntity(this)
		this.addComponent(model)
		this.addComponent(transform)

		this.addComponent(new Animator())
		this.getComponent(Animator).addClip(new AnimationState('RatIdle2', { looping: true }))
		this.getComponent(Animator).addClip(new AnimationState('RatRun', { looping: true }))
		this.endPath = endRatPath
		this.stop()
	}

	private run() {
		this.getComponent(Animator).getClip('RatRun').play()
	}

	public stop() {
		this.getComponent(Animator).getClip('RatIdle').play()
	}

	public turnLeft() {
		this.getComponent(Transform).rotate(new Vector3(0, 1, 0), -90)
	}

	public turnRight() {
		this.getComponent(Transform).rotation.setEuler(0, 90, 0)
	}

	private moveRat(path: Path3D, speed: number) {
		const pathData = new PathData(path)
		this.addComponent(pathData)

		this.patrolSystem = new PatrolPath(this, pathData, speed)
		engine.addSystem(this.patrolSystem)

		// TODO: Refactor
		GameDataInstance.addSystem(this.patrolSystem)
	}

	public addTrigger() {
		let triggerBoxShape = new utils.TriggerBoxShape(new Vector3(10, 5, 10), Vector3.Zero())
		this.addComponent(
			new utils.TriggerComponent(triggerBoxShape, {
				onCameraEnter: () => {
					if (!this.patrolSystem) {
						this.run()
						if (this.endPath) {
							this.moveRat(this.endPath, 3)
						}
					}
				}
			})
		)
	}
}

@Component('pathData')
export class PathData {
	origin: Vector3
	target: Vector3
	path: Path3D
	fraction = 0
	nextPathIndex = 1

	constructor(path: Path3D) {
		this.path = path
		this.origin = path.path[0]
		this.target = path.path[1]
	}
}

export class PatrolPath implements ISystem {
	private rat: Rat
	private pathData: PathData
	private speed: number
	constructor(rat: Rat, pathData: PathData, speed: number) {
		this.rat = rat
		this.speed = speed
		this.pathData = pathData
	}
	update(dt: number) {
		let transform = this.rat.getComponent(Transform)
		let path = this.rat.getComponent(PathData)

		if (path.fraction < 1) {
			transform.position = Vector3.Lerp(path.origin, path.target, path.fraction)
			path.fraction += dt / this.speed
		} else {
			this.rat.turnLeft()
			path.nextPathIndex += 1
			if (path.nextPathIndex >= this.pathData.path.path.length) {
				path.nextPathIndex = 0
			}
			path.origin = path.target
			path.target = this.pathData.path.path[path.nextPathIndex]
			path.fraction = 0
		}
	}
}
