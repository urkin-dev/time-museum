export class Catapult extends Entity {
	constructor(model: GLTFShape, transform: TranformConstructorArgs) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		this.addComponent(transform)

		this.addComponent(new Animator())
		const shootAnimation = new AnimationState('CatapultShoot', { looping: false })

		this.getComponent(Animator).addClip(shootAnimation)
		this.addComponent(new OnPointerDown(() => shootAnimation.play()))
	}
}
