import * as utils from '@dcl/ecs-scene-utils'

export class Elevator extends Entity {
	constructor(model: GLTFShape, transform: TranformConstructorArgs) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		this.addComponent(transform)
	}
}
