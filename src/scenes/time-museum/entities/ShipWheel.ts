export class ShipWheel extends Entity {
	constructor(model: GLTFShape, transform: Transform) {
		super()
		engine.addEntity(this)
		this.addComponent(model)
		this.addComponent(transform)

		this.addComponent(new Animator())
		this.getComponent(Animator).addClip(new AnimationState('WheelRotate1', { looping: false }))
		this.getComponent(Animator).addClip(new AnimationState('WheelRotate2', { looping: false }))

		this.addComponent(
			new OnPointerDown(() => {
				const probability = Math.random() < 0.5

				if (probability) {
					this.getComponent(Animator).getClip('WheelRotate2').play()
				} else {
					this.getComponent(Animator).getClip('WheelRotate1').play()
				}
			})
		)
	}
}
