import * as utils from '@dcl/ecs-scene-utils'

type moveDirectionType = 'left' | 'right'

export class ElevatorDoor extends Entity {
	constructor(
		model: GLTFShape,
		transform: TranformConstructorArgs,
		direction: moveDirectionType,
		speed = 0.5,
		delta: Vector3
	) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		this.addComponent(new Transform(transform))

		this.addComponent(
			new utils.ToggleComponent(utils.ToggleState.Off, (value): void => {
				if (value === utils.ToggleState.On) {
					this.addComponentOrReplace(
						new utils.MoveTransformComponent(this.getComponent(Transform).position, delta, speed)
					)
				} else {
					this.addComponentOrReplace(
						new utils.MoveTransformComponent(this.getComponent(Transform).position, Vector3.Zero(), speed)
					)
				}
			})
		)
	}

	closeDoor() {
		this.getComponent(utils.ToggleComponent).set(utils.ToggleState.Off)
	}

	openDoor() {
		this.getComponent(utils.ToggleComponent).set(utils.ToggleState.On)
	}
}
