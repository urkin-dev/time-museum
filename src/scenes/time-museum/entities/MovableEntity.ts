import * as utils from '@dcl/ecs-scene-utils'

export class MovableEntity extends Entity {
	constructor(model: GLTFShape, transform: TranformConstructorArgs, deltaPosition: Vector3, duration: double) {
		super()
		engine.addEntity(this)

		this.addComponent(model)
		this.addComponent(new Transform(transform))

		const startPos: any = transform.position
		const endPos: any = transform.position?.add(deltaPosition)

		this.addComponent(
			new utils.ToggleComponent(utils.ToggleState.Off, (value): void => {
				if (value === utils.ToggleState.On) {
					this.addComponentOrReplace(
						new utils.MoveTransformComponent(this.getComponent(Transform).position, endPos, duration)
					)
				} else {
					this.addComponentOrReplace(
						new utils.MoveTransformComponent(this.getComponent(Transform).position, startPos, duration)
					)
				}
			})
		)
	}
}
