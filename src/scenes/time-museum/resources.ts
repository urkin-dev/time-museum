const DEFAULT_MODELS_PATH = `models/time-museum`

export default {
	models: {
		// EGypt
		palm: new GLTFShape(`${DEFAULT_MODELS_PATH}/egypt/palm.glb`),
		coconut: new GLTFShape(`${DEFAULT_MODELS_PATH}/egypt/coconut.glb`),
		egyptAge: new GLTFShape(`${DEFAULT_MODELS_PATH}/egypt/egyptAge.glb`),

		// Age of dicovery
		chest: new GLTFShape(`${DEFAULT_MODELS_PATH}/ageOfDiscovery/chest.glb`),
		wheelShip: new GLTFShape(`${DEFAULT_MODELS_PATH}/ageOfDiscovery/wheelShip.glb`),
		discoveryAge: new GLTFShape(`${DEFAULT_MODELS_PATH}/ageOfDiscovery/discoveryAge.glb`),
		compass: new GLTFShape(`${DEFAULT_MODELS_PATH}/ageOfDiscovery/compass.glb`),

		// Greek
		trojanHorse: new GLTFShape(`${DEFAULT_MODELS_PATH}/greek/trojanHorse.glb`),
		greekAge: new GLTFShape(`${DEFAULT_MODELS_PATH}/greek/greekAge.glb`),

		// Medieval
		cannon: new GLTFShape(`${DEFAULT_MODELS_PATH}/medieval/cannon.glb`),
		catapult: new GLTFShape(`${DEFAULT_MODELS_PATH}/medieval/catapult.glb`),
		sword: new GLTFShape(`${DEFAULT_MODELS_PATH}/medieval/sword.glb`),
		medievalAge: new GLTFShape(`${DEFAULT_MODELS_PATH}/medieval/medievalAge.glb`),

		// Stone age
		stoneAge: new GLTFShape(`${DEFAULT_MODELS_PATH}/stoneAge/stoneAge.glb`),

		// All Ages
		bonfire: new GLTFShape(`${DEFAULT_MODELS_PATH}/Bonfire.glb`),
		rat: new GLTFShape(`${DEFAULT_MODELS_PATH}/rat.glb`),

		// Elevator
		borderEgypt: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/borderAncientEgypt.glb`),
		borderGreece: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/borderGreece.glb`),
		borderMiddleAges: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/borderMiddleAges.glb`),
		borderStoneAge: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/borderStoneAge.glb`),
		borderDiscovery: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/borderDiscovery.glb`),
		elevatorCabin: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/elevator_cabin.glb`),
		elevatorDoor1: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/door1.glb`),
		elevatorDoor2: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/door2.glb`),
		button: new GLTFShape(`${DEFAULT_MODELS_PATH}/elevator/button.glb`),

		// Ground
		grass: new GLTFShape(`${DEFAULT_MODELS_PATH}/ground/FloorGrass.glb`),
		grass2: new GLTFShape(`${DEFAULT_MODELS_PATH}/ground/FloorGrass2.glb`),
		sand: new GLTFShape(`${DEFAULT_MODELS_PATH}/ground/FloorSand.glb`),
		sea: new GLTFShape(`${DEFAULT_MODELS_PATH}/ground/FloorSea.glb`),
		snow: new GLTFShape(`${DEFAULT_MODELS_PATH}/ground/FloorSnow.glb`)
	}
}
