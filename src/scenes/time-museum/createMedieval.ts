import * as utils from '@dcl/ecs-scene-utils'
import { BaseEntity, Rat, Cannon, Sword } from './entities/index'
import { GameDataInstance } from './store/GameData'
import resources from './resources'

export function createMedieval(offsetVecor: Vector3) {
	const ground = new BaseEntity(
		resources.models.grass2,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(ground)

	const startPath = new Vector3(10, 0, 12).add(offsetVecor)
	const endRatPath = new Path3D([
		startPath,
		new Vector3(22, 0, 12).add(offsetVecor),
		new Vector3(22, 0, 22).add(offsetVecor),
		new Vector3(10, 0, 22).add(offsetVecor)
	])
	const rat = new Rat(
		resources.models.rat,
		new Transform({ position: startPath, scale: new Vector3(0.2, 0.2, 0.2) }),
		endRatPath
	)
	rat.addTrigger()
	rat.turnRight()

	GameDataInstance.addEntity(rat)

	const medievalAge = new BaseEntity(
		resources.models.medievalAge,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(medievalAge)

	const sword = new Sword(
		resources.models.sword,
		new Transform({
			position: new Vector3(27.6, -0.2, 25.6).add(offsetVecor),
			scale: new Vector3(0.5, 0.5, 0.5)
		})
	)
	GameDataInstance.addEntity(sword)

	// Remove the sword when user leave scene
	let triggerBoxShape = new utils.TriggerBoxShape(new Vector3(32, 10, 32), new Vector3(16, 0, 16))

	ground.addComponent(
		new utils.TriggerComponent(triggerBoxShape, {
			onCameraExit: () => {
				if (sword.isGrabbed) {
					GameDataInstance.removeEntity(sword)
				}
			}
		})
	)

	const catapult = new BaseEntity(
		resources.models.catapult,
		new Transform({
			position: new Vector3(16, 0.3, 3).add(offsetVecor),
			// rotation: new Quaternion(0, 1, 0),
			scale: new Vector3(0.6, 0.6, 0.6)
		})
	)
	catapult.addComponent(new Animator())
	const shootAnimation = new AnimationState('CatapultShoot', { looping: false })
	catapult.getComponent(Animator).addClip(shootAnimation)
	catapult.addComponent(new OnPointerDown(() => shootAnimation.play()))
	GameDataInstance.addEntity(catapult)

	const cannon = new Cannon(
		resources.models.cannon,
		{
			position: new Vector3(2, 1.6, 2.5).add(offsetVecor),
			rotation: new Quaternion(0, -3, 0),
			scale: new Vector3(0.3, 0.3, 0.3)
		},
		new Vector3(3, 0, 3),
		3
	)
	GameDataInstance.addEntity(cannon)
}
