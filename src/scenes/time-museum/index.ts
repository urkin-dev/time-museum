import { createEgypt } from './createEgypt'
import { createGreek } from './createGreek'
import { createStoneAge } from './createStoneAge'
import { createAgeOfDiscovery } from './createAgeOfDiscovery'
import { createMedieval } from './createMedieval'

export { createEgypt, createGreek, createStoneAge, createAgeOfDiscovery, createMedieval }
