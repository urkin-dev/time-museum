import { BaseSceneBuilder } from './BaseScene'

export function createTimeMuseum() {
	const sceneBuilder = new BaseSceneBuilder()
	sceneBuilder.build()
}
