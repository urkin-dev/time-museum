import { BaseEntity, Coconut } from './entities/index'
import { GameDataInstance } from './store/GameData'
import resources from './resources'

export function createEgypt(offsetVecor: Vector3) {
	const ground = new BaseEntity(
		resources.models.sand,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(ground)

	const egyptAge = new BaseEntity(
		resources.models.egyptAge,
		new Transform({ position: new Vector3(0, 0, 0).add(offsetVecor) })
	)
	GameDataInstance.addEntity(egyptAge)

	const palm1 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(20, 0, 5).add(offsetVecor),
			scale: new Vector3(1.2, 1.2, 1.2)
		})
	)
	palm1.getComponent(Transform).rotation.setEuler(0, 0, 0)
	GameDataInstance.addEntity(palm1)

	const coconut1 = new Coconut(
		resources.models.coconut,
		new Transform({ position: new Vector3(-0.8, 3.4, 0), scale: new Vector3(0.3, 0.3, 0.3) }),
		new Vector3(0, -3.3, 0),
		0.5
	)
	coconut1.setParent(palm1)

	const palm2 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(24, 0, 3).add(offsetVecor),
			scale: new Vector3(1, 1, 1)
		})
	)
	palm2.getComponent(Transform).rotation.setEuler(0, 90, 0)
	const coconut2 = new Coconut(
		resources.models.coconut,
		new Transform({ position: new Vector3(-0.8, 3.4, 0), scale: new Vector3(0.3, 0.3, 0.3) }),
		new Vector3(0, -3.3, 0),
		0.5
	)
	coconut2.setParent(palm2)

	GameDataInstance.addEntity(palm2)

	const palm3 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(19, 0, 2).add(offsetVecor),
			rotation: new Quaternion(0, 0.7, 0),
			scale: new Vector3(0.7, 0.7, 0.7)
		})
	)
	palm3.getComponent(Transform).rotation.setEuler(0, 140, 0)

	GameDataInstance.addEntity(palm3)

	const palm4 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(26, 0, 18).add(offsetVecor),
			scale: new Vector3(1, 1, 1)
		})
	)
	palm4.getComponent(Transform).rotation.setEuler(0, 5, 0)
	GameDataInstance.addEntity(palm4)

	const palm5 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(29, 0, 18).add(offsetVecor),
			rotation: new Quaternion(0, -0.2, 0),
			scale: new Vector3(0.7, 0.7, 0.7)
		})
	)
	palm5.getComponent(Transform).rotation.setEuler(0, 50, 0)
	GameDataInstance.addEntity(palm5)

	const palm6 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(16, 0, 30).add(offsetVecor),
			rotation: new Quaternion(0, -1, 0),
			scale: new Vector3(0.7, 0.7, 0.7)
		})
	)
	GameDataInstance.addEntity(palm6)

	const palm7 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(18, 0, 28).add(offsetVecor),
			scale: new Vector3(1, 1, 1)
		})
	)
	palm7.getComponent(Transform).rotation.setEuler(0, 18, 0)
	GameDataInstance.addEntity(palm7)

	const palm8 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(20, 0, 25).add(offsetVecor),
			scale: new Vector3(0.7, 0.7, 0.7)
		})
	)
	palm8.getComponent(Transform).rotation.setEuler(0, 30, 0)
	GameDataInstance.addEntity(palm8)

	const palm9 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(3, 0, 29).add(offsetVecor),
			rotation: new Quaternion(0, -0.5, 0),
			scale: new Vector3(0.9, 0.9, 0.9)
		})
	)
	palm9.getComponent(Transform).rotation.setEuler(0, 100, 0)
	GameDataInstance.addEntity(palm9)

	const palm10 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(4, 0, 26).add(offsetVecor),
			scale: new Vector3(1.2, 1.2, 1.2)
		})
	)
	palm10.getComponent(Transform).rotation.setEuler(0, 80, 0)
	GameDataInstance.addEntity(palm10)

	const palm11 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(5, 0, 29).add(offsetVecor),
			scale: new Vector3(0.7, 0.7, 0.7)
		})
	)
	palm11.getComponent(Transform).rotation.setEuler(0, 10, 0)
	GameDataInstance.addEntity(palm11)

	const palm12 = new BaseEntity(
		resources.models.palm,
		new Transform({
			position: new Vector3(7, 0, 26).add(offsetVecor),
			scale: new Vector3(1, 1, 1)
		})
	)
	GameDataInstance.addEntity(palm12)
}
