module.exports = {
	root: true,
	extends: ['plugin:import/recommended', 'plugin:import/typescript'],
	parser: '@typescript-eslint/parser',
	rules: {
		'comma-dangle': 'off',
		'semi': 'off',
		'import/no-unresolved': 'off',
		'import/namespace': 'off',
		'import/default': 'off'
	}
}
